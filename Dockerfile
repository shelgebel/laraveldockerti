FROM ubuntu:20.04
SHELL ["/bin/bash", "-c"]
LABEL maintaine='shelgebel@gmail.com'
RUN  apt update -y && apt install -y software-properties-common  && add-apt-repository ppa:ondrej/php &&  apt update  -y &&  apt install -y  php7.4 php7.4-mysql php7.4-mbstring php7.4-xml php7.4-bcmath
